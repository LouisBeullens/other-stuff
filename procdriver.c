#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/types.h>
#include <asm/io.h>
#include <asm/uaccess.h>

#define MODULE_NAME "procdriver: "

const char leds[] = { 17, 18, 27, 22, 23, 24, 14, 4 };

volatile uint32_t *base = NULL;
volatile uint32_t *fsel0 = NULL;
volatile uint32_t *fsel1 = NULL;
volatile uint32_t *fsel2 = NULL;

static ssize_t hello_proc_read(struct file *file, char __user *buffer, size_t count, loff_t *data)
{
	uint32_t levels = *(base + (0x200034 >> 2));
	char procfs_buffer[] = { 0, 0, 0, 0, 0, 0, 0, 0};
	int i;

	printk(KERN_DEBUG MODULE_NAME "hello_proc_read.\n");

	if (*data == 0)
	{
		for (i = 0; i < 8; i++)
		{
			procfs_buffer[7-i] = (levels & (1 << leds[i])) ? '1': '0';
		}

		if (copy_to_user(buffer, procfs_buffer, 8)) {
			return -EFAULT;
		}

		*data += 8;
		return 8;
	}
	else
	{
		*data = 0;
		return 0;
	}
}

static ssize_t hello_proc_write(struct file *file, const char __user *buffer, size_t count, loff_t *data)
{
	char procfs_buffer[8];
	ssize_t size = (count <= 8) ? count : 8;
	uint32_t set = 0;
	uint32_t clr = 0;
	int i;

	printk(KERN_DEBUG MODULE_NAME "hello_proc_write.\n");

	if (copy_from_user(procfs_buffer, buffer, size)) {
		return -EFAULT;
	}

	for (i = 0; i < size; i++)
	{
		if (procfs_buffer[size-i-1] == '1')
		{
			set |= 1 << leds[i];
		}
		else if (procfs_buffer[size-i-1] == '0')
		{
			clr |= 1 << leds[i];
		}
	}

	__sync_synchronize();
	*fsel0 = (*fsel0 & 0x3fff8fff) | 0x00001000;
	__sync_synchronize();
	*fsel1 = (*fsel1 & 0x381f8fff) | 0x01201000;
	__sync_synchronize();
	*fsel2 = (*fsel2 & 0x3f1f803f) | 0x00201240;
	__sync_synchronize();
	*(base+(0x20001c>>2)) = set;
	__sync_synchronize();
	*(base+(0x200028>>2)) = clr;
	__sync_synchronize();

	return size;
}

static const struct file_operations hello_proc_fops = {
	.owner = THIS_MODULE,
	.read = hello_proc_read,
	.write = hello_proc_write,
};

static int __init hello_proc_init(void) {
	proc_create("procdriver", 0666, NULL, &hello_proc_fops);
	printk(KERN_DEBUG MODULE_NAME "init procdriver!\n");
	base = ioremap(0x3f000000, 0x1000000);
	fsel0 = (base + (0x200000 >> 2));
	fsel1 = (base + (0x200004 >> 2));
	fsel2 = (base + (0x200008 >> 2));
	return 0;
}

static void __exit hello_proc_exit(void) {
	remove_proc_entry("procdriver", NULL);
	iounmap(base);
	printk(KERN_DEBUG MODULE_NAME "exit procdriver!\n");
}

MODULE_LICENSE("GPL");
module_init(hello_proc_init);
module_exit(hello_proc_exit);